FROM alpine:latest

LABEL org.label-schema.docker.dockerfile=/Dockerfile org.label-schema.vcs-type=Git org.label-schema.vcs-url=https://gitlab.com/aazario/docker-duckdns.git

ARG TARGETARCH

RUN apk add --no-cache curl

COPY ./scripts /scripts

WORKDIR /scripts

RUN chmod -R +x /scripts

CMD ["/bin/sh", "/scripts/update.sh"]