# Duck DNS Updater

Updates your Duck DNS IP address automatically at the frequency specified. Use `alpine` as a minimal base image for performing `curl` requests.

## Variables

* `DUCKDNS_TOKEN`: Duck DNS account token (obtained from [Duck DNS](https://www.duckdns.org))
* `DUCKDNS_DOMAIN`: Full Duck DNS domain (e.g. `foo.duckdns.org`), you can also pass a comma separated (no spaces) list of domains (e.g. `foo.duckdns.org,bar.duckdns.org`)
* `DUCKDNS_IP`: You can if you need to hard code an IP (best not to - leave it blank and they detect your remote ip)
* `DUCKDNS_DELAY`: Delay between IP address updates (by default 5 minutes)

## Usage

```
docker run \
    -e DUCKDNS_TOKEN=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX \
    -e DUCKDNS_DOMAIN=foo.duckdns.org \
    -e DUCKDNS_DELAY=5 \
    -d aazario/duckdns
```