#!/bin/sh

# Check variables DUCKDNS_TOKEN, DUCKDNS_DOMAIN, DUCKDNS_DELAY
if [ -z "$DUCKDNS_TOKEN" ]; then
   echo "ERROR: Variable DUCKDNS_TOKEN is unset"
   exit 1
fi

if [ -z "$DUCKDNS_DOMAIN" ]; then
    echo "ERROR: Variable DUCKDNS_DOMAIN is unset"
    exit 1
fi

if [ -z "$DUCKDNS_DELAY" ]; then
    DUCKDNS_DELAY=5
fi

# Print variables
echo "DUCKDNS_TOKEN: $DUCKDNS_TOKEN"
echo "DUCKDNS_DOMAIN: $DUCKDNS_DOMAIN"
echo "DUCKDNS_IP: $DUCKDNS_IP"
echo "DUCKDNS_DELAY: $DUCKDNS_DELAY minute(s)"

# Suggest longer delay if below 5 minutes
if [ "$DUCKDNS_DELAY" -lt 5 ]; then
    echo "WARNING: Consider using a delay of at least 5 minutes"
fi

while :; do
    echo url="https://www.duckdns.org/update?domains=${DUCKDNS_DOMAIN}&token=${DUCKDNS_TOKEN}&ip=${DUCKDNS_IP}" | curl -ks -o /scripts/duck.log -K -

    if [ ! -f "/scripts/duck.log" ]; then
        echo "ERROR: Failed to create Duck DNS log file, check your TOKEN and DOMAIN"
        exit 1
    fi

    if [ "$(cat /scripts/duck.log)" != "OK" ]; then
        echo "ERROR: IP address update failed, check your TOKEN and DOMAIN"
        exit 1
    fi

    echo "Successfully updated IP address"
    echo "Sleeping for $DUCKDNS_DELAY minute(s)"
    sleep $((${DUCKDNS_DELAY} * 60))
done
